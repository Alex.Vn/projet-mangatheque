<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230116104815 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer DROP FOREIGN KEY FK_81398E09AC2B36D0');
        $this->addSql('DROP INDEX UNIQ_81398E09AC2B36D0 ON customer');
        $this->addSql('ALTER TABLE customer DROP mangatheque_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer ADD mangatheque_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09AC2B36D0 FOREIGN KEY (mangatheque_id) REFERENCES mangatheque (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_81398E09AC2B36D0 ON customer (mangatheque_id)');
    }
}
