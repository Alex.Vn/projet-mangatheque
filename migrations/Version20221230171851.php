<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221230171851 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE mangatheque_product (mangatheque_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_6F50EA74AC2B36D0 (mangatheque_id), INDEX IDX_6F50EA744584665A (product_id), PRIMARY KEY(mangatheque_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mangatheque_product ADD CONSTRAINT FK_6F50EA74AC2B36D0 FOREIGN KEY (mangatheque_id) REFERENCES mangatheque (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mangatheque_product ADD CONSTRAINT FK_6F50EA744584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE customer ADD mangatheque_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09AC2B36D0 FOREIGN KEY (mangatheque_id) REFERENCES mangatheque (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_81398E09AC2B36D0 ON customer (mangatheque_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mangatheque_product DROP FOREIGN KEY FK_6F50EA74AC2B36D0');
        $this->addSql('ALTER TABLE mangatheque_product DROP FOREIGN KEY FK_6F50EA744584665A');
        $this->addSql('DROP TABLE mangatheque_product');
        $this->addSql('ALTER TABLE customer DROP FOREIGN KEY FK_81398E09AC2B36D0');
        $this->addSql('DROP INDEX UNIQ_81398E09AC2B36D0 ON customer');
        $this->addSql('ALTER TABLE customer DROP mangatheque_id');
    }
}
