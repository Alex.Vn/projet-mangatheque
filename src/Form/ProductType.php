<?php

namespace App\Form;

use App\Entity\Genre;
use App\Entity\Author;
use App\Entity\Product;
use App\Entity\Publisher;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('price')
            ->add('valid')
            ->add('pictureUpload', FileType::class, [
                'mapped' => false,
                "constraints" => [
                    new File([
                        "maxSize" => "10M",
                        "mimeTypes" => [
                            "image/png",
                            "image/jpg",
                            "image/jpeg",
                            "image/gif"
                        ],
                        "mimeTypesMessage" => "Veuillez envoyer une image au format png, jpg, jpeg ou gif, de 10 mégas octets maximum"
                        ])
                ]
            ])
            
            ->add('author', EntityType::class, [             
                'class' => Author::class,
                'choice_label' => 'name',           
            ])
            ->add('genre', EntityType::class, [             
                'class' => Genre::class,
                'choice_label' => 'name',           
            ])
            ->add('publisher', EntityType::class, [             
                'class' => Publisher::class,
                'choice_label' => 'name',           
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
