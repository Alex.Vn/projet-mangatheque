<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/cart", name="cart_")
 */
#[Route("/cart", name:"add")]
class CartController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    #[Route("/add/{id}", name:"add")]
    public function add(Product $product, SessionInterface $session)
    {
        // On récupère le panier actuel
        $panier = $session->get("panier", []);
        $id = $product->getId();

        if(!empty($panier[$id])){
            $panier[$id]++;
        }else{
            $panier[$id] = 1;
        }

        // On sauvegarde dans la session
        $session->set("panier", $panier);

        return $this->redirectToRoute("cart/index.html.twig");
    }

    /**
     * @Route("/remove/{id}", name="remove")
     */
    #[Route("/remove/{id}", name:"remove")]
    public function remove(Product $product, SessionInterface $session)
    {
        // On récupère le panier actuel
        $panier = $session->get("panier", []);
        $id = $product->getId();
        var_dump($panier);
        var_dump($panier[$id]);

        // if(!empty($panier[$id])){
        //     if($panier[$id] > 1){
        //         $panier[$id]--;
        //     }else{
        //         unset($panier[$id]);
        //     }
        // }

        // On sauvegarde dans la session
        $session->set("panier", $panier);

        return $this->redirectToRoute("cart/index.html.twig");
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    #[Route("/delete/{id}", name:"delete")]
    public function delete(Product $product, SessionInterface $session)
    {
        // On récupère le panier actuel
        $panier = $session->get("panier", []);
        $id = $product->getId();

        if(!empty($panier[$id])){
            unset($panier[$id]);
        }

        // On sauvegarde dans la session
        $session->set("panier", $panier);

        return $this->redirectToRoute("cart/index.html.twig");
    }

    /**
     * @Route("/delete", name="delete_all")
     */
    #[Route("/delete", name:"delete_all")]
    public function deleteAll(SessionInterface $session)
    {
        $session->remove("panier");

        return $this->redirectToRoute("cart/index.html.twig");
    }
}