<?php

namespace App\Controller;

use App\Entity\Publisher;
use App\Form\PublisherType;
use App\Service\FileUploader;
use App\Repository\PublisherRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/publisher')]
class AdminPublisherController extends AbstractController
{
    #[Route('/', name: 'app_admin_publisher_index', methods: ['GET'])]
    public function index(PublisherRepository $publisherRepository): Response
    {
        return $this->render('admin_publisher/index.html.twig', [
            'publishers' => $publisherRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_publisher_new', methods: ['GET', 'POST'])]
    public function new(Request $request, PublisherRepository $publisherRepository, FileUploader $fileUploader, SluggerInterface $slugger): Response
    {
        $publisher = new Publisher();
        $form = $this->createForm(PublisherType::class, $publisher);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pictureFile = $form->get('pictureUpload')->getData();
            if ($pictureFile) {
                $pictureFileName = $fileUploader->upload('publisher/', $pictureFile);
                $publisher->setPicture($pictureFileName);
                $publisher->setSlug($slugger->slug($publisher->getName())->lower()); 
                $publisherRepository->save($publisher, true);
            }
           

            return $this->redirectToRoute('app_admin_publisher_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_publisher/new.html.twig', [
            'publisher' => $publisher,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_publisher_show', methods: ['GET'])]
    public function show(Publisher $publisher): Response
    {
        return $this->render('admin_publisher/show.html.twig', [
            'publisher' => $publisher,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_publisher_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Publisher $publisher, PublisherRepository $publisherRepository): Response
    {
        $form = $this->createForm(PublisherType::class, $publisher);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $publisherRepository->save($publisher, true);

            return $this->redirectToRoute('app_admin_publisher_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_publisher/edit.html.twig', [
            'publisher' => $publisher,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_publisher_delete', methods: ['POST'])]
    public function delete(Request $request, Publisher $publisher, PublisherRepository $publisherRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$publisher->getId(), $request->request->get('_token'))) {
            $publisherRepository->remove($publisher, true);
        }

        return $this->redirectToRoute('app_admin_publisher_index', [], Response::HTTP_SEE_OTHER);
    }
}
