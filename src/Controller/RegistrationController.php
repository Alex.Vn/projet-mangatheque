<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use DateTimeImmutable;
use App\Entity\Customer;
use App\Form\CustomerType;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Repository\CustomerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class RegistrationController extends AbstractController
{
    #[Route('/register', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setCreatedAt(new DateTimeImmutable('now'));
            $user->setValid(true);
            $user->setRoles(["ROLE_CUSTOMER"]);
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
          

            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('app_login');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
    #[Route('/edit/customer', name: 'app_register_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request,UserRepository $userRepository): Response
   
    {   
        $customer = $this->getUser()->getCustomer();
        $user = $this->getUser();
        $form = $this->createForm(CustomerType::class, $customer);
        // $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userRepository->save($user, true);
            return $this->redirectToRoute('order', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('registration/edit.html.twig', [
            'form' => $form,
            // 'user' => $user,
            'customer' => $customer,
        ]); 
    } 

    #[Route('/edit/user', name: 'app_register_edit_user', methods: ['GET', 'POST'])]
    public function editUser(Request $request, UserRepository $userRepository): Response
   
    {   
        $user = $this->getUser();
        // $form = $this->createForm(UserType::class, $user);
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

       
        if ($form->isSubmitted() && $form->isValid()) {
            
            $userRepository->save($user, true);
            return $this->redirectToRoute('app_home_account', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('registration/editUser.html.twig', [
            'registrationForm' => $form->createView(),
            'user' => $user,
            'edit' => $edit = false,
            
        ]); 
    } 
}
      

