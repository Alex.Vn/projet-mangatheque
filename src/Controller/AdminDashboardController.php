<?php

namespace App\Controller;

use App\Entity\Author;
use App\Form\AuthorType;
use App\Service\FileUploader;
use App\Repository\GenreRepository;
use App\Repository\AuthorRepository;
use App\Repository\ProductRepository;
use App\Repository\CustomerRepository;
use App\Repository\OrderRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;






class AdminDashboardController extends AbstractController
{
    #[Route('/admin/dashboard', name: 'app_admin_dashboard', methods: ['GET'])]
    public function index(Request $request, GenreRepository $genreRepository, AuthorRepository $authorRepository, ProductRepository $productRepository, CustomerRepository $customerRepository, OrderRepository $orderRepository): Response
    {
        return $this->render('admin_dashboard/index.html.twig', [
            'controller_name' => 'AdminDashboardController',
            'products' => $productRepository->findBy(
                array(),
                array('id' => 'DESC'),
                5,
                0
            ),
            'authors' => $authorRepository->findBy(
                array(),
                array('id' => 'DESC'),
                5,
                0
            ),
            'genres' => $genreRepository->findBy(
                array(),
                array('id' => 'DESC'),
                5,
                0
            ),
            'customers' => $customerRepository->findBy(
                array(),
                array('id' => 'DESC'),
                5,
                0
            ),
            'orders' => $orderRepository->findBy(
                array(),
                array('id' => 'DESC'),
                5,
                0
            ),
        ]);
    }
}