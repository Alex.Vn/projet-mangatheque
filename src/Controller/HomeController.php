<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Order;
use App\Entity\Author;
use App\Entity\Product;
use App\Entity\Customer;
use App\Entity\Mangatheque;
use App\Entity\OrderDetail;
use App\Repository\UserRepository;
use App\Repository\OrderRepository;

use App\Repository\AuthorRepository;
use App\Repository\ProductRepository;
use App\Repository\CustomerRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\MangathequeRepository;
use App\Repository\OrderDetailRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class HomeController extends AbstractController
{
    
    #[Route('/', name: 'app_home')]
    public function index(AuthenticationUtils $authenticationUtils,ProductRepository $productRepository): Response
    {   
        $lastUsername = $authenticationUtils->getLastUsername();
        
        
        // dd($lastUsername);
        
        return $this->render('home/index.html.twig', [
            'products' => $productRepository->findAll()
        ]);
    }

    #[Route('/collection', name: 'app_home_collection')]
    public function collection(SessionInterface $session): Response
    {
        
        // dd($session->getId());
        $mangatheque = $this->getUser()->getCustomer()->getMangatheque();
        $mangathequeCount = $this->getUser()->getCustomer()->getMangatheque()->count();
       
        
       


        return $this->render('home/collection.html.twig', [
            'controller_name' => 'HomeController',
            'mangatheque' => $mangatheque,
            'mangathequeCount' => $mangathequeCount,
        ]);
    }

    #[Route('/recherche', name: 'app_home_recherche')]
    public function recherche(): Response
    {
        return $this->render('home/recherche.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    #[Route('/account', name: 'app_home_account')]
    public function account(): Response
    {
        $userId = $this->getUser()->getId();
        return $this->render('home/account.html.twig', [
            'controller_name' => 'HomeController',
            'userId' => $userId,
        ]);
    }
    #[Route('/account-commande', name: 'app_home_account_commande')]
    public function accountcommande(UserRepository $userRepository,OrderRepository $orderRepository, OrderDetailRepository $orderDetailRepository): Response
    {
       
        
        $customer = $this->getUser()->getCustomer()->getOrders();
        // $test = $this->getUser()->getCustomer()->getOrders()->getOrderDetails();
        // dd($test);
        return $this->render('home/account-commande.html.twig', [
            // 'user' => $user,
            'customer' => $customer,
            
        ]);
    }
    // #[Route('/page-produit/{id}', name: 'app_home_page_produit', methods: ['GET'])]
    // public function pageProduit(ProductRepository $productRepository, Product $product): Response
    // {
    //     return $this->render('home/page-produit.html.twig', [
    //         'product' => $product,
    //         // 'products' => $productRepository->getId(),
    //     ]);
    // }

    #[Route('/page-produit/{slug}', name: 'app_home_page_produit', methods: ['GET'])]
    public function pageProduit(ProductRepository $productRepository, Product $product, CustomerRepository $customerRepository): Response
    {

        
        // $customer = $this->getUser()->getCustomer();
        $customer = null;
        $user = $this->getUser();
        if ($user && $user->getCustomer()) {
            $customer = $user->getCustomer();
        }

       
        return $this->render('home/page-produit.html.twig', [
            'product' => $product,
            'customer' => $customer,
        ]);
    }

    #[Route('/page-detail/{slug}', name: 'app_home_page_detail', methods: ['GET'])]
    public function pageDetail(ProductRepository $productRepository, Product $product): Response
    {
        $customer = $this->getUser()->getCustomer();
        // $authorId = $product->getAuthor()->getId();
        $author = $product->getAuthor();
        // dd($authorId);
        // $authorId = $product->getAuthor();
        
        
        return $this->render('home/page-detail-mangatheque.html.twig', [
            'product' => $product,
            'customer' => $customer,
            // 'authorId' => $authorId,
            'author' => $author,
        ]);
    }





    #[Route('/panier', name: 'app_home_panier')]
    public function panier(SessionInterface $session): Response
    {
      
        return $this->render('home/panier.html.twig', [
            'panier' => $session->get("panier", [])
        ]);
    }

    #[Route("/addMangatheque/{customer}/{product}", name:"addProductMangatheque")]
    public function addMangatheque(Customer $customer,Product $product, EntityManagerInterface $entityManager)
    
    { 
        $customer->addMangatheque($product);
        $entityManager->persist($customer); 
        $entityManager->flush();
        $mangatheque = $customer->getMangatheque();
            
        return $this->redirectToRoute("app_home_collection");
        // return $this->render("home/collection.html.twig", [
        //     'mangatheque' => $customer->getMangatheque()          
        // ]);        
    }

    #[Route("/removeMangatheque/{customer}/{product}", name:"removeProductMangatheque")]
    public function removeMangatheque(Customer $customer,Product $product, EntityManagerInterface $entityManager)
    
    { 
        $customer->removeMangatheque($product);
        $entityManager->persist($customer); 
        $entityManager->flush();
      
            
        return $this->redirectToRoute("app_home_collection");
        // return $this->render("home/collection.html.twig", [
        //     'mangatheque' => $customer->getMangatheque()          
        // ]);        
    }





    // PANIER //    
    #[Route("panier/add/{id}", name:"addProduct")]
    public function add(Product $product, SessionInterface $session)
  
    {      
        //récupère le panier actuel, si le panier existe pas on le créer
        $panier = $session->get("panier", []);
        
        //récupère l'id du produit
        $id = $product->getId();

        //si le produit existe déjà dans le panier on increment sa quantité
        if(!empty($panier[$id])){
            $panier[$id]['quantity']++;
           
            
        }else{
            // si le produit n'existe pas déjà dans le panier on ajoute le produit et met la quantité à 1
            $panier[$id]['product'] = $product;
            $panier[$id]['quantity'] = 1;
            
            
        }
           
        // sauvegarde dans la session
        $session->set("panier", $panier);
        // redirection sur la page panier    
        return $this->redirectToRoute("app_home_panier");
    }

   
    #[Route("panier/remove/{id}", name:"remove")]
    public function remove(Product $product, SessionInterface $session)
    {
        // On récupère le panier actuel
        $panier = $session->get("panier", []);
        $id = $product->getId();       
        if(!empty($panier[$id])){
            if($panier[$id]['quantity'] > 1){
                $panier[$id]['quantity']--;
            }else{
                unset($panier[$id]);
            }
        }
        // On sauvegarde dans la session
        $session->set("panier", $panier);
        return $this->redirectToRoute("app_home_panier");
    }

    
    #[Route("/delete/{id}", name:"delete")]
    public function delete(Product $product, SessionInterface $session)
    {
        // On récupère le panier actuel
        $panier = $session->get("panier", []);
        $id = $product->getId();
        if(!empty($panier[$id])){
            unset($panier[$id]);
        }
        // On sauvegarde dans la session
        $session->set("panier", $panier);
        return $this->redirectToRoute("app_home_panier");
    }
   
    #[Route("/delete", name:"delete_all")]
    public function deleteAll(SessionInterface $session)
    {
        $session->remove("panier");
        return $this->redirectToRoute("cart_index");
    }

    #[Route("/order", name:"order")]
    public function order(SessionInterface $session)
    {       
        $panier = $session->get("panier", []);
        $userId = $this->getUser()->getId();
        $customerId = $this->getUser()->getCustomer()->getId();
              
        // dd($panier);
        return $this->render('order/order.html.twig', [
            'panier' => $panier,
            'userId' => $userId,
            'customerId' => $customerId,
            
        ]);
    }

    #[Route('/author/{id}', name: 'app_author_manga', methods: ['GET'])]
    public function allManga(Author $author,AuthorRepository $authorRepository, ProductRepository $productRepository): Response

    
    {
    //    
        $manga = $author->getProduct();
       
    //    $mangaAuthor = $productRepository->findBy('author_id', $)
    //    $test = $authorRepository->findBy(['id' => $]);
        
        return $this->render('home/author.html.twig', [
            'manga' => $manga,
            'author' => $author,
            
        ]);
    }

}