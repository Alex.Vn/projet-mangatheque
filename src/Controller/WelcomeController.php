<?php

namespace App\Controller;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;



class WelcomeController extends AbstractController
{
    
    #[Route('/welcome', name: 'app_welcome')]
    public function welcome(AuthenticationUtils $authenticationUtils): Response
    {   
        
        
        
        // dd($lastUsername);
        
        return $this->render('home/welcome.html.twig', [
        ]);
    }
}    