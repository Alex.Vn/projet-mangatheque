<?php

namespace App\Controller;

use App\Entity\Mangatheque;
use App\Form\MangathequeType;
use App\Repository\MangathequeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/mangatheque')]
class AdminMangathequeController extends AbstractController
{
    #[Route('/', name: 'app_admin_mangatheque_index', methods: ['GET'])]
    public function index(MangathequeRepository $mangathequeRepository): Response
    {
        return $this->render('admin_mangatheque/index.html.twig', [
            'mangatheques' => $mangathequeRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_mangatheque_new', methods: ['GET', 'POST'])]
    public function new(Request $request, MangathequeRepository $mangathequeRepository): Response
    {
        $mangatheque = new Mangatheque();
        $form = $this->createForm(MangathequeType::class, $mangatheque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mangathequeRepository->save($mangatheque, true);

            return $this->redirectToRoute('app_admin_mangatheque_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_mangatheque/new.html.twig', [
            'mangatheque' => $mangatheque,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_mangatheque_show', methods: ['GET'])]
    public function show(Mangatheque $mangatheque): Response
    {
        return $this->render('admin_mangatheque/show.html.twig', [
            'mangatheque' => $mangatheque,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_mangatheque_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Mangatheque $mangatheque, MangathequeRepository $mangathequeRepository): Response
    {
        $form = $this->createForm(MangathequeType::class, $mangatheque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mangathequeRepository->save($mangatheque, true);

            return $this->redirectToRoute('app_admin_mangatheque_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_mangatheque/edit.html.twig', [
            'mangatheque' => $mangatheque,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_mangatheque_delete', methods: ['POST'])]
    public function delete(Request $request, Mangatheque $mangatheque, MangathequeRepository $mangathequeRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$mangatheque->getId(), $request->request->get('_token'))) {
            $mangathequeRepository->remove($mangatheque, true);
        }

        return $this->redirectToRoute('app_admin_mangatheque_index', [], Response::HTTP_SEE_OTHER);
    }
}
