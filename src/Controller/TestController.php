<?php

namespace App\Controller;

use App\Entity\Order;
use DateTimeImmutable;
use App\Entity\OrderDetail;
use App\Repository\OrderRepository;
use App\Repository\CustomerRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\OrderDetailRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TestController extends AbstractController
{
    #[Route('/confirmation', name: 'confirmation', methods: ['GET', 'POST'])]
    public function new(CustomerRepository $customerRepository,SessionInterface $session,OrderRepository $orderRepository, OrderDetailRepository $orderDetailRepository, EntityManagerInterface $entityManager): Response
    {
        $panier = $session->get("panier", []);
        $customer = $this->getUser()->getCustomer();
        // $customer = $customerRepository->find();
        // ->getUser()->getCustomer()->getId()
        // dd($customerId);
        $order = new Order();

          
            $order->setCreatedAt(new DateTimeImmutable('now'));
            $order->setShippedAt(new DateTimeImmutable('now'));
            $order->setDeliveryAt(new DateTimeImmutable('now'));
            $order->setPaidAt(new DateTimeImmutable('now'));
            $order->setShippingCost(3);
            $order->setStatus('in process');

            
            foreach ($panier as $element) {
                $orderDetail = new OrderDetail();
                $produit = $element['product']->getPrice();
                $quantity = $element['quantity'];
                $orderDetail->setQuantity($quantity);
                $orderDetail->setPriceEach($produit);
                $order->addOrderDetail($orderDetail);
                $order->setCustomer($customer);
                $orderDetail->setOrders($order);
                $entityManager->persist($orderDetail);
            }        
            $entityManager->persist($order);
            $entityManager->flush();
    
            $session->remove("panier");
          
           
           
            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        // }
    }
}
