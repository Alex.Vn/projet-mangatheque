<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Form\CustomerType;
use App\Form\EditFormType;
use App\Form\RegistrationFormType;
use App\Repository\CustomerRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;




 class CustomerRegistrationController extends AbstractController
 {


#[Route('{id}/edit', name: 'app_register_edit_test', methods: ['GET', 'POST'])]
public function editCustomer(Request $request, CustomerRepository $customerRepository, Customer $customer): Response

{   
    
    $form = $this->createForm(CustomerType::class, $customer);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $customerRepository->save($customer, true);
        return $this->redirectToRoute('order', [], Response::HTTP_SEE_OTHER);
    }

    return $this->render('registration/edit.html.twig', [
        'Form' => $form->createView(),
        'customer' => $customer,
        'form' => $form,
    ]); 
}
}